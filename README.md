AptMirror
=========

This role will set up an apt mirror repository of the Ubuntu Apt repos for a distro you specify. It will also mirror third-party repos and host them on the same mirror.

This role does not 'host' the repo - you will need to set up and configure a webserver yourself (nginx or Apache).

Third-party content
-------------------

The Apt Mirror binary was obtianed from [this repo](https://github.com/Stifler6996/apt-mirror). The reason for using this binary instead of the one included with Ubuntu (focal) is due to its lack of ability to sync C-N-F files as documented [here](https://github.com/apt-mirror/apt-mirror/issues/145). At this time the [official aptmirror repo](https://github.com/apt-mirror/apt-mirror) appears to have been abandoned.

Requirements
------------

The role itself does not have any requirements. The Apt Mirror binary is included with the role.

The role will require a minimum of 500GB of disk space (recommended 1TB). it is recommended to add a second disk (or virtual disk) to your host and mount this in the location where your apt mirror will be located. 

Role Variables
--------------

All variables are located under the aptmirror dictionary (so service.user is aptmirror.service.user)

| Variable                  | Type   | Required | Default Value       | Description |
| --------                  | ----   | -------- | ------------------- | ----------- |
| service.user              | string | no       | aptmirror           |
| service.group             | string | no       | aptmirror           |
| service.root              | string | no       | /opt/aptmirror/apt  |
| service.cron.minute       | int    | no       | 00                  |
| service.cron.hour         | int    | no       | 01                  |
| content.distros.src       | bool   | no       | false               |
| content.distros.security  | bool   | no       | true                |
| content.distros.updates   | bool   | no       | true                |
| content.distros.backports | bool   | no       | false               |
| content.distros.releases  | list   | no       | focal         |
| content.extrarepos        | list   | no       | (see below)         |

| Variable    | Type   | Required | Example Value       |
| --------    | ----   | -------- | ------------------- |
| name        | string | yes      | 'Hashicorp Vault' |
| deb         | string | yes      | 'deb [arch={{ architecture_mapping }}] https://apt.releases.hashicorp.com focal main' |
| gpgsrc      | string | yes      | 'https://apt.releases.hashicorp.com/gpg' |
| gpgoutfile  | string | yes      | 'hashicorp-archive-keyring.gpg' |

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Deploy aptmirror role and configure to mirror Focal and Hashicorp Vault
      hosts: aptmirror
      become: true
      gather_facts: true
      roles:
        - role: 'aptmirror'
      vars:
        aptmirror:
          service:
            user: aptmirror
            group: aptmirror
            root: /opt/aptmirror/apt
            cron:
              minute: 00
              hour: 01
          content:
            distros:
              backports: true
              src: false
              security: true
              updates: true
              releases:
                - focal
            extrarepos:
              - name: 'Hashicorp Vault'
                deb: 'deb [arch={{ architecture_mapping }}] https://apt.releases.hashicorp.com focal main'
                gpgsrc: 'https://apt.releases.hashicorp.com/gpg'
                gpgoutfile: 'hashicorp-archive-keyring.gpg'

License
-------

Apache-2.0

Author Information
------------------

[Tristan Findley](https://tfindley.co.uk)

Want to show your thanks? [Buy me a Coffee!](https://www.buymeacoffee.com/tristan)